import { configureStore } from '@reduxjs/toolkit';
import personReducer from './stateManagement/personReducer';


const store = configureStore({
    reducer: {
        // Регистрируем два редюсера
        // Под порядковый номер
        person: personReducer,
    },

});
export default store;