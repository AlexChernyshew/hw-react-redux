import { Person } from "../../../models/person";
import PersonItem from "./Item";

interface Props {

}

const list: Person[] = [
    {
        username: "1",
        nickname: 'Jade',
        color: 'red',
    },
    {
        username: "2",
        nickname: 'Cage',
        color: 'yellow',
    },
    {
        username: "3",
        nickname: 'Lao',
        color: 'green',
    },
    {
        username: "4",
        nickname: 'Zero',
        color: 'blue',
    },

    {
        username: "5",
        nickname: 'Wolf',
        color: 'cyan',
    },
    {
        username: "6",
        nickname: 'Bane',
        color: 'purple',
    },
]

const PersonLister = (props: Props) => {
    const css = {
        padding: '10px',
    };
    return <div className='container' style={css}>
        {list.map(x => <PersonItem item={x} key={`oi_${x.nickname}`} />)}
    </div >;
};

export default PersonLister;